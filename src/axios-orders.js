import axios from 'axios';

let axiosInstance = axios.create({
    baseURL : 'https://my-burger-builder-app-e452e.firebaseio.com/'
});

export default axiosInstance;