import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import classes from './Auth.css';
import * as actionCreators from '../../store/actions/actionIndex';
import Spinner from '../../components/UI/Spinner/Spinner';
import AuthenticationFailed from '../../components/Auth/AuthenticationFailed';
import { Redirect } from 'react-router-dom';
import { updatedObject, checkValidity } from '../../shared/utility';

class Auth extends Component {

    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true,

                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6,

                },
                valid: false,
                touched: false
            }
        },
        isSignUp: true,
        isFormValid: false
    }

    componentDidMount() {
        if (!this.props.buildingBurger && this.props.authRedirectPath !== '/') {
            this.props.onSetAuthRedirectPath()
        }
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControlElements = updatedObject(this.state.controls[controlName], {
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
            touched: true
        });
        const updatedControls = updatedObject(this.state.controls, {
            [controlName]: updatedControlElements
        });
        let isFormValid = true;
        for (let inputIdentifier in updatedControls) {
            isFormValid = updatedControls[inputIdentifier].valid && isFormValid;
        }
        this.setState({ controls: updatedControls, isFormValid: isFormValid });
    }

    submitHandler = (event) => {
        event.preventDefault();
        this.props.onCheckAuthenticate(this.state.controls.email.value,
            this.state.controls.password.value,
            this.state.isSignUp);
    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return { isSignUp: !prevState.isSignUp };
        });
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }
        let form = formElementsArray.map(formElement => {
            return (
                <Input
                    key={formElement.id}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    value={formElement.config.value}
                    invalid={!(formElement.config.valid)}
                    shouldValidate={formElement.config.validation}
                    touched={formElement.config.touched}
                    changed={(event) => this.inputChangedHandler(event, formElement.id)}
                />
            );
        });

        if (this.props.loading) {
            form = <Spinner />;
        }

        let errorMessge = null;

        if (this.props.error) {
            errorMessge = <AuthenticationFailed errorType={this.props.error.message} />
        }
        let authRedirect = null;
        if (this.props.isAuthenticated) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }

        return (
            <div className={classes.Auth}>
                {authRedirect}
                <form onSubmit={this.submitHandler}>
                    {form}
                    {errorMessge}
                    <Button
                        buttonType='Success'
                        disabled={!this.state.isFormValid}> SUBMIT </Button>
                </form>
                <Button
                    clicked={this.switchAuthModeHandler}
                    buttonType='Danger'> SWITCH TO {this.state.isSignUp ? 'SIGN IN' : 'SIGN UP'} </Button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token,
        buildingBurger: state.builderReducer.building,
        authRedirectPath: state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCheckAuthenticate: (email, password, isSignUp) => dispatch(actionCreators.authenticationProcess(email, password, isSignUp)),
        onSetAuthRedirectPath: () => dispatch(actionCreators.setAuthRedirectPath('/'))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);