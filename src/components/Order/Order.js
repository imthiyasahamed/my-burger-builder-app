import React from 'react';
import classes from './Order.css';

const order = (props) => {
    const ingredients = [];
    for (let ingredientName in props.ingredients) {
        ingredients.push({
            name: ingredientName,
            quantity: props.ingredients[ingredientName]
        });
    }

    const ingredientOutput = ingredients.map(value => {
        return <span
            key={value.name}
            className={classes.Span}>{value.name} ({value.quantity})</span>;
    });
    return (
        <div className={classes.Order}>
            <p> Ingredients : {ingredientOutput} </p>
            <p> Price : <strong>USD {props.price.toFixed(2)}</strong></p>
        </div>
    );
};

export default order;