import React, { Component } from 'react';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import Button from '../../UI/Button/Button';

class OrderSummary extends Component {

    render() {
        const ingredientsSummary = Object.keys(this.props.ingredients).map(
            elementKey => {
                return (
                    <li key={elementKey}>
                        <span style={{ textTransform: 'capitalize' }}>{elementKey}</span> : {this.props.ingredients[elementKey]}
                    </li>
                );
            });
        return (
            <Aux>
                <h3>Your Order</h3>
                <p>A Delicious Burger with the following ingredients :</p>
                <ul>
                    {ingredientsSummary}
                </ul>
                <p><strong>Total Price : ${this.props.price.toFixed(2)} </strong></p>
                <p>Continue to Checkout ?</p>
                <Button buttonType="Danger" clicked={this.props.purchaseCancelled}>CANCEL</Button>
                <Button buttonType="Success" clicked={this.props.purchaseContinued}>CONTINUE</Button>
            </Aux>
        );
    }
};

export default OrderSummary;