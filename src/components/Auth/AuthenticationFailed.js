import React from 'react';

const authenticationFailed = (props) => {

    switch (props.errorType) {
        case 'Network Error':
            return <p style={{ textAlign: 'center', color: 'red' }}>Check Your Network Connection.</p>;
        case 'EMAIL_NOT_FOUND':
            return <p style={{ textAlign: 'center', color: 'red' }}>Mail Address Not Found</p>;
        case 'INVALID_PASSWORD':
            return <p style={{ textAlign: 'center', color: 'red' }}>Incorrect Password.</p>;
        case 'USER_DISABLED':
            return <p style={{ textAlign: 'center', color: 'red' }}>Your Account is Disabled Please Contact Administrator.</p>;
        case 'EMAIL_EXISTS':
            return <p style={{ textAlign: 'center', color: 'red' }}>E-Mail Already Exists.</p>;
        case 'OPERATION_NOT_ALLOWED':
            return <p style={{ textAlign: 'center', color: 'red' }}>Password Sign-in is Disabled for this Application</p>;
        case 'TOO_MANY_ATTEMPTS_TRY_LATER':
            return <p style={{ textAlign: 'center', color: 'red' }}>Too Many Attempts Try Again Later.</p>;
        case 'MISSING_PASSWORD':
            return <p style={{ textAlign: 'center', color: 'red' }}>Please Enter Password</p>;
        case 'MISSING_EMAIL':
            return <p style={{ textAlign: 'center', color: 'red' }}>Please Enter Mail Address</p>;
        case 'INVALID_EMAIL':
            return <p style={{ textAlign: 'center', color: 'red' }}>Invalid Email.</p>;
        default:
            return props.errorType;
    }
}

export default authenticationFailed;