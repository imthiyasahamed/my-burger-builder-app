import * as actionTypes from '../actions/actionTypes';
import { updatedObject } from '../../shared/utility';

const initialState = {
    orders: [],
    loading: false,
    purchased: false
}

const purchaseInit = state => {
    return updatedObject(state, { purchased: false });
}

const startProcess = state => {
    return updatedObject(state, { loading: true });
}

const purchaseBurgerSuccess = (state, action) => {
    const newOrder = updatedObject(action.orderData, { id: action.orderId });
    return updatedObject(state, {
        loading: false,
        orders: state.orders.concat(newOrder),
        purchased: true
    });
}

const failProcess = state => {
    return updatedObject(state, { loading: false });
}

const fetchOrderSuccess = (state, action) => {
    return updatedObject(state, {
        orders: action.orders,
        loading: false
    });
}

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.PURCHASE_INIT: return purchaseInit(state);
        case actionTypes.PURCHASE_BURGER_START
            || actionTypes.FETCH_ORDERS_START: return startProcess(state);
        case actionTypes.PURCHASE_BURGER_SUCCESS: return purchaseBurgerSuccess(state, action);
        case actionTypes.PURCHASE_BURGER_FAIL
            || actionTypes.FETCH_ORDERS_FAIL: return failProcess(state);
        case actionTypes.FETCH_ORDERS_SUCCESSS: return fetchOrderSuccess(state, action);
        default: return state;
    }
}

export default orderReducer;