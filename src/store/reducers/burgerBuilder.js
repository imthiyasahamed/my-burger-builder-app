import * as actionTypes from '../actions/actionTypes';
import { updatedObject } from '../../shared/utility';

const initialState = {
    ingredients: null,
    totalPrice: 4,
    error: false,
    building: false
}

const INGREDIENT_PRICES = {
    salad: 0.5,
    bacon: 0.7,
    cheese: 0.4,
    meat: 1.3,
};

const setIncredients = (state, action) => {
    return updatedObject(state, {
        ingredients: {
            salad: action.ingredients.salad,
            bacon: action.ingredients.bacon,
            cheese: action.ingredients.cheese,
            meat: action.ingredients.meat
        },
        totalPrice: 4,
        building: false
    });
}

const fetchIngredientsFailed = (state) => {
    return updatedObject(state, { error: !state.error });
}

const addIncredient = (state, action) => {
    const addedIngredient = { [action.ingredientName]: state.ingredients[action.ingredientName] + 1 };
    const addedIngredients = updatedObject(state.ingredients, addedIngredient);
    const ingredientsAddedState = {
        ingredients: addedIngredients,
        totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
        building: true
    }
    return updatedObject(state, ingredientsAddedState);
}

const removeIncredient = (state, action) => {
    const removedIngredient = { [action.ingredientName]: state.ingredients[action.ingredientName] - 1 };
    const removedIngredients = updatedObject(state.ingredients, removedIngredient);
    const ingredientsRemovedState = {
        ingredients: removedIngredients,
        totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName],
        building: true
    }
    return updatedObject(state, ingredientsRemovedState);
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_INGREDIENTS: return setIncredients(state, action)
        case actionTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientsFailed(state);
        case actionTypes.ADD_INGREDIENT: return addIncredient(state, action);
        case actionTypes.REMOVE_INGREDIENT: return removeIncredient(state, action);
        default: return state;
    }
}

export default reducer;